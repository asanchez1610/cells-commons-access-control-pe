import { setDocumentCustomStyles, } from '@bbva-web-components/bbva-core-lit-helpers';
import { css, } from 'lit-element';

setDocumentCustomStyles(css`
  #iframeBody {
    margin: 0;
  }
  div {
    line-height: 26px;
    padding: 15px;
    font-family: sans-serif;
  }
`);
